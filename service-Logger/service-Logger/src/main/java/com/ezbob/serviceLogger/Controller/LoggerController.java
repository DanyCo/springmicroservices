package com.ezbob.serviceLogger.Controller;

import com.ezbob.serviceLogger.serviceLogger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class LoggerController {

    @RequestMapping("/PrintLog/{log}")
    ResponseEntity printlog(@PathVariable String log) {
        serviceLogger.printlog(log);
        ResponseEntity r= new ResponseEntity(log, HttpStatus.OK);
        return r;
    }
}
