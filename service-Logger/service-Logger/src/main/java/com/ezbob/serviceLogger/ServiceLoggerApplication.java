package com.ezbob.serviceLogger;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ServiceLoggerApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServiceLoggerApplication.class, args);
	}

}
