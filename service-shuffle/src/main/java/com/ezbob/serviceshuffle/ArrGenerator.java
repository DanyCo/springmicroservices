package com.ezbob.serviceshuffle;

import java.util.*;

public  class ArrGenerator {

    public static int [] buildArr(int num) {
        ArrayList<Integer> map = init(num);
        int[] arr = new int[num];
        int pos=0;
        for (int index = num ; index > 0 ; index--)
        {
            int res = generateRandomInt(index);
            int value = map.get(res);
            map.remove(res);
            arr[pos]=value;
            pos++;
        }
       return arr;

    }

    private static int generateRandomInt(int upperRange){
        Random random = new Random();
        return random.nextInt(upperRange);
    }

    private static ArrayList<Integer> init(int number) {
        ArrayList<Integer> d = new ArrayList<>();
        for (Integer  index = 0;index<number;index++ )
        {
            d.add(index+1);
        }
        return d;
    }
}
