package com.ezbob.serviceshuffle.Controller;

import com.ezbob.serviceshuffle.ArrGenerator;
import com.ezbob.serviceshuffle.serviceLogger;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.concurrent.CompletableFuture;

@RestController

public class ShuffleController {
    @Autowired
    private ConfigurableEnvironment myEnv;

    @PostMapping("/Number")
    ResponseEntity<Gson> getArr(@RequestBody String numberAsString) {
        int number = Integer.parseInt(numberAsString);
        int[] res =  ArrGenerator.buildArr(number);
        CompletableFuture.runAsync(()->{
            serviceLogger.printlog(number,myEnv.getProperty("ServiceLoggerPath"));
        });

        return new ResponseEntity(res, HttpStatus.OK);
    }








}
