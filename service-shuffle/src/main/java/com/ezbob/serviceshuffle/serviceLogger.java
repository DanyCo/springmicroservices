package com.ezbob.serviceshuffle;

import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;


public class serviceLogger {

    public static void printlog(int number , String path)
    {
        String log = String.valueOf(number);
        String baseUrl  = path +"/"+log;
        ResponseEntity<String> obj=  new RestTemplate().getForEntity(baseUrl,String.class);
        System.out.println("the requested number was "+number+" send it to the Logger Service-> response Code: "+obj.getStatusCode()+ " :) ");
    }
}
