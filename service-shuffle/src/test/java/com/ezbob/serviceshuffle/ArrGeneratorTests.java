package com.ezbob.serviceshuffle;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;
import java.util.Arrays;

@SpringBootTest
public class ArrGeneratorTests {
@Test
    public void buildArrTest()
    {
        int testValue = 1000;
        int[] res =  ArrGenerator.buildArr(testValue);
        long r = Arrays.stream(res).distinct().count();
        Assert.assertEquals(testValue,r);
    }

}
